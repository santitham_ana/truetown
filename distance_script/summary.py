import csv
import os
import math
import copy
import sys
import click
import clipboard
from utils import read_csv
from utils import write_to_csv
from config import ROOT_PATH
from distance import run_cal_distance

def filter_data_distance(data, filter_distance=1000):
    BMA = 0
    UPC = 0
    BLOCK_ID = 0
    WONGNAI = 0

    BMA_TRUETOWN = set()
    UPC_TRUETOWN = set()
    filter_data = []
    for row in data:
        if  float(row['NEAREST_DISTANCE']) > filter_distance:
            continue
        if 'BMA' in row['SELECT_TRUETOWN_BLOCK_NAME']:
            BMA_TRUETOWN.add(row['SELECT_TRUETOWN_BLOCK_NAME'])
        else:
            UPC_TRUETOWN.add(row['SELECT_TRUETOWN_BLOCK_NAME'])

        filter_data.append(row)
        BLOCK_ID += 1
        WONGNAI += float(row['MERCHANT_COUNT_WONGNAI'])
    
    print("Filter distance = {}".format(filter_distance))
    print("BMA :",len(BMA_TRUETOWN))
    print("UPC :",len(UPC_TRUETOWN))
    print("BLOCK ID :{}".format(BLOCK_ID))
    print("WONGNAI :{}".format(WONGNAI))

    return filter_data

def receive_input(mode):
    filename = input('Please input filename: ')
    if mode == 1:
        distance = int(input('Please select filter distance (M): '))
        return filename, distance
    else:
        return filename

def get_truetown_unique_name(filename):
    source_file = os.path.join(ROOT_PATH, 'source', filename)
    data = read_csv(source_file)
    results = set()
    for item in data:
        if item['TRUETOWN_BLOCK_NAME'] != '':
            results.add(item['TRUETOWN_BLOCK_NAME'])
    return list(results)

def run_summary(filename, distance):
    full_path = os.path.join(ROOT_PATH, 'distance_output', filename)
    source_file = os.path.join(ROOT_PATH, 'source', filename)
    run_cal_distance(source_file)
        
    data = read_csv(full_path)
    results = filter_data_distance(data, distance)
    return results

def count_truetown_increase_block(data):
    truetown = {}
    total_block_id = 0
    total_wongnai = 0
    total_already_truetown = 0
    total_already_wongnai = 0
    for item in data:
        key = item['SELECT_TRUETOWN_BLOCK_NAME']
        if item['TRUETOWN_BLOCK_NAME'] != '':
            total_already_truetown += 1
            total_already_wongnai += int(item['MERCHANT_COUNT_WONGNAI'])
        if key != '' and key not in truetown:
            truetown[key] = {
                'block_id': 0,
                'wongnai': 0
            }
        select = item['SELECT_TRUETOWN_BLOCK_NAME']
        truetown[select]['block_id'] += 1
        truetown[select]['wongnai'] += int(item['MERCHANT_COUNT_WONGNAI'])
        total_block_id += 1
        total_wongnai += int(item['MERCHANT_COUNT_WONGNAI'])
        if item['TRUETOWN_BLOCK_NAME'] != '':
            truetown[select]['block_id'] -= 1
            truetown[select]['wongnai'] -= int(item['MERCHANT_COUNT_WONGNAI'])
    results = []
    for item in truetown:
        results.append({
            'name': item,
            'count_block_id': truetown[item]['block_id'],
            'count_wongnai': truetown[item]['wongnai']
        })
    total_data = {
        'total_inc_block_id':  total_block_id,
        'total_already_truetown': total_already_truetown,
        'total_inc_wongnai': total_wongnai,
        'total_already_wongnai': total_already_wongnai
    }
    return results, total_data

@click.command()
@click.option('--interactive', default=False, is_flag=True)
@click.option('--clipboard_mode', default=False, is_flag=True)
def main(interactive, clipboard_mode):
    if interactive:
        mode = int(input('Which mode do you prefer 1) Summary 2) Truetown Name List: '))
        if mode == 1:
            filename, distance = receive_input(mode)
            data = run_summary(filename, distance)
            print("---------------------------------------------------------------------------------")
            results, total_data = count_truetown_increase_block(data)
            print('Added Block ID {}, Already Truetown Block ID {}, Added Wongnai {}, Already Truetown Wongnai {}'.format(
                total_data['total_inc_block_id'], 
                total_data['total_already_truetown'],
                total_data['total_inc_wongnai'],
                total_data['total_already_wongnai']),
            )
            string_results = 'Already Truetown,{}\n'.format(total_data['total_already_truetown'])
            for item in results:
                print('{},{},{}'.format(item['name'], item['count_block_id'], item['count_wongnai']))
                string_results += '{},{},{}\n'.format(item['name'], item['count_block_id'], item['count_wongnai'])
                if clipboard_mode:
                    clipboard.copy(string_results)
        else:
            filename = receive_input(mode)
            data = get_truetown_unique_name(filename)
            string_results = ''
            for item in data:
                print(item)
                string_results += '{}\n'.format(item)
            if clipboard_mode:
                clipboard.copy(string_results)
    else:
        # for filename in ['high_segment.csv', 'accumulate.csv', 'percentile.csv']:
        headers = ['Distance' , 'Criteria', 'Truetown', 'Total Added Block', 'Total Added Wongnai']
        truetown_list = []
        # for filename in ['high_segment.csv', 'criteria2.csv', 'criteria3.csv']:
        for filename in ['criteria3.csv']:
            for distance in [1000, 2000, 3000, 5000, 10000]:
            # for distance in [1000]:
                print('Solution {}'.format(filename.replace('.csv', '')))
                data = run_summary(filename, distance)
                results, total_data = count_truetown_increase_block(data)
                print("---------------------------------------------------------------------------------")
                for item in results:
                    criteria = filename.split('.csv')[0]
                    temp_dict = {
                        'Distance': distance,
                        'Criteria': criteria,
                        'Truetown': item['name'],
                        'Total Added Block': item['count_block_id'],
                        'Total Added Wongnai': item['count_wongnai']
                    }
                    truetown_list.append(temp_dict)
        write_to_csv(truetown_list, os.path.join(ROOT_PATH, 'distance_output', 'truetown_list.csv'), headers)

if __name__ == "__main__":
    main()
