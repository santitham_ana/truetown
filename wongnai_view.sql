WITH OVERVIEW AS (
  SELECT 
    * REPLACE(
      CASE
        WHEN TRUETOWN_BLOCK_NAME IS NOT NULL THEN TRUETOWN_BLOCK_NAME
        WHEN TRUETOWN_BLOCK_NAME IS NULL AND PKOI_BLOCK_NAME IS NULL THEN SELECT_TRUETOWN_BLOCK_NAME
        WHEN TRUETOWN_BLOCK_NAME IS NULL AND PKOI_BLOCK_NAME IS NOT NULL THEN PKOI_BLOCK_NAME
        -- WHEN TRUETOWN_BLOCK_NAME IS NULL AND NEAREST_DISTANCE > 3000 AND PKOI_BLOCK_NAME IS NOT NULL THEN PKOI_BLOCK_NAME
        ELSE SELECT_TRUETOWN_BLOCK_NAME
      END AS TRUETOWN_BLOCK_NAME
    )
  FROM (
    SELECT 
    A.*
    ,B.TRUETOWN_BLOCK_ID AS PKOI_BLOCK_ID
    ,B.TRUETOWN_BLOCK_NAME AS PKOI_BLOCK_NAME
    FROM `acm-bi.Analysis.NW_TRUETOWN_HIGH_SEGMENT_ADDITION` A
    LEFT JOIN (
      SELECT *
      FROM `acm-bi.Analysis.NW_TRUETOWN_PKOI_ADDITION`
      WHERE TRUETOWN_BLOCK_NAME NOT LIKE '%BMA 20%' AND TRUETOWN_BLOCK_NAME NOT LIKE '%BMA 27%'
    ) B
    ON A.TRUETOWN_BLOCK_ID = B.TRUETOWN_BLOCK_ID
    WHERE NEAREST_DISTANCE <= 3000 OR B.TRUETOWN_BLOCK_NAME IS NOT NULL AND A.TRUETOWN_BLOCK_ID IS NOT NULL
  )
),
WONGNAI AS (
  SELECT * EXCEPT(RANK)
  FROM (
    SELECT
      name AS wongnai_name
      ,phone_no
      ,province_name
      ,district_name
      ,city_name
      ,address
      ,lat AS latitude
      ,lng AS longitude
      ,category
      ,CASE
        WHEN potential IS NULL THEN 'UNKNOWN'
        ELSE potential
      END AS potential
      ,CASE
        WHEN price_group IS NULL THEN 'UNKNOWN'
        ELSE price_group
      END AS price_group
      ,CASE
        WHEN SELECT_TRUETOWN_BLOCK_NAME	IS NOT NULL THEN 'HIGH SEGMENT'
        ELSE 'UNKNOWN'
      END AS truetown_segment
      ,TRUETOWN_BLOCK_NAME AS truetown_block_name
      ,TRUETOWN_BLOCK_ID AS truetown_block_id
      ,CONCAT('https://www.google.com/maps/search/?api=1&query=', lat, ',', lng) AS googlemap_link
      ,ROW_NUMBER() OVER(PARTITION BY name, CAST(lat AS STRING), CAST(lng AS STRING) ORDER BY name, lat, lng) AS RANK
    FROM `acm-bi.Analysis.NW_TRUETOWN_WONGNAI_SOURCE`
    WHERE TRUETOWN_BLOCK_ID IN (
      SELECT TRUETOWN_BLOCK_ID
      FROM `acm-bi.Analysis.NW_TRUETOWN_WITH_INACTIVE`
      WHERE ACTIVE_MERCHANT_COUNT > 0
    )
  )
  WHERE RANK = 1 AND wongnai_name IS NOT NULL
),
TARGET AS (
    SELECT 
    *
    ,CASE
        WHEN A_BLOCK_ID IS NOT NULL AND A_B_BLOCK_ID IS NULL AND B_BLOCK_ID IS NULL THEN 'TMN'
        WHEN A_BLOCK_ID IS NULL AND A_B_BLOCK_ID IS NULL AND B_BLOCK_ID IS NOT NULL THEN 'TRUEYOU'
        ELSE 'TMN&TRUEYOU'
    END AS block_id_from
    FROM WONGNAI A
    LEFT JOIN (
        SELECT DISTINCT TRUETOWN_BLOCK_ID AS A_BLOCK_ID
        FROM OVERVIEW
        WHERE NEAREST_DISTANCE <= 3000 AND PKOI_BLOCK_NAME IS NULL
        ORDER BY 1
    ) TMN ON A.TRUETOWN_BLOCK_ID = A_BLOCK_ID
    LEFT JOIN (
    SELECT DISTINCT PKOI_BLOCK_ID AS A_B_BLOCK_ID
    FROM OVERVIEW
    WHERE NEAREST_DISTANCE <= 3000 AND PKOI_BLOCK_NAME IS NOT NULL
    ORDER BY 1
    ) TMN_AND_TRUEYOU ON A.TRUETOWN_BLOCK_ID = A_B_BLOCK_ID
    LEFT JOIN (
    SELECT DISTINCT PKOI_BLOCK_ID AS B_BLOCK_ID
    FROM OVERVIEW
    WHERE PKOI_BLOCK_NAME IS NOT NULL AND NEAREST_DISTANCE > 3000
    ORDER BY 1
    ) TRUEYOU ON A.TRUETOWN_BLOCK_ID = B_BLOCK_ID
)

SELECT
  A.*
  ,CASE
    WHEN B.key IS NOT NULL THEN 'KA'
    WHEN B.key IS NULL AND price_group = 'PREMIUM' THEN 'KA'
    WHEN B.key IS NULL AND price_group = 'BUDGET' THEN 'LONGTAIL'
    WHEN B.key IS NULL AND price_group = 'MODERATE' THEN 'LONGTAIL'
    ELSE 'UNKNOWN'
  END AS merchant_type
FROM (
  SELECT B.key,A.*
  FROM (
    SELECT DISTINCT *
    FROM TARGET
  ) A
  LEFT JOIN `acm-bi.Analysis.NW_TRUETOWN_WONGNAI_KEY` B
  ON A.wongnai_name = B.wongnai_name AND A.latitude = B.latitude AND A.longitude = B.longitude
) A
LEFT JOIN `acm-bi.Analysis.NW_WONGNAI_EXIST_KA_LIST` B
ON A.key = B.key