with 
without_wongnai as (
  select
  TDS_REGION,
  TDS_PROVINCE,
  TRUETOWN_BLOCK_NAME,
  TRUETOWN_BLOCK_ID,
  sum(MAU) as MAU_RETAIL,
  sum(MAU_7) as MAU_7,
  sum(MAU_KA) as MAU_KA,
  sum(MAU_LT) as MAU_LT,
  count(*) as MERCHANT_COUNT,
  sum(QUALITY_MERCHANT) as QUALITY_MERCHANT_COUNT,
  sum(FLAG_7) as MERCHANT_COUNT_7,
  sum(FLAG_KA) as MERCHANT_COUNT_KA,
  sum(FLAG_LT) as MERCHANT_COUNT_LT,
  sum(QUALITY_MERCHANT_7) as QUALITY_MERCHANT_COUNT_7,
  sum(QUALITY_MERCHANT_KA) as QUALITY_MERCHANT_COUNT_KA,
  sum(QUALITY_MERCHANT_LT) as QUALITY_MERCHANT_COUNT_LT,
  sum(TRUETOWN_FLAG) as IN_TRUETOWN_COUNT,
  sum(BLOCK_FLAG) as HAVE_BLOCK_ID_COUNT
  from (  
    select 
    *,
    if(BRAND_NAME = '7-ELEVEN', 1, 0) as FLAG_7,
    if(BRAND_NAME <> '7-ELEVEN' and MERCHANT_TYPE = 'KA', 1, 0) as FLAG_KA,
    if(MERCHANT_TYPE = 'NON-KA', 1, 0) as FLAG_LT,
    if(MAU >= 5, 1, 0) as QUALITY_MERCHANT,
    if(BRAND_NAME = '7-ELEVEN' and MAU >= 5, 1, 0) as QUALITY_MERCHANT_7,
    if(BRAND_NAME <> '7-ELEVEN' and MERCHANT_TYPE = 'KA' and MAU >= 5, 1, 0) as QUALITY_MERCHANT_KA,
    if(MERCHANT_TYPE = 'NON-KA' and MAU >= 5, 1, 0) as QUALITY_MERCHANT_LT,
    from (
        select 
        TDS_REGION,
        TDS_PROVINCE,
        TRUETOWN_BLOCK_NAME,
        TRUETOWN_BLOCK_ID,
        MERCHANT_ID,
        SHOP_ID,
        BRAND_NAME,
        MERCHANT_TYPE,
        TRUETOWN_FLAG,
        BLOCK_FLAG,
        avg(MAU) as MAU,
        avg(MAU_7) as MAU_7,
        avg(MAU_KA) as MAU_KA,
        avg(MAU_LT) as MAU_LT
        from (
            -- Count MAU each merchant (36157 count)
            select 
            TDS_REGION,
            TDS_PROVINCE,
            TRUETOWN_BLOCK_NAME,
            TRUETOWN_BLOCK_ID,
            MERCHANT_ID,
            SHOP_ID,
            BRAND_NAME,
            MERCHANT_TYPE,
            if(TRUETOWN_BLOCK_NAME is not null, 1, 0) as TRUETOWN_FLAG,
            if(TRUETOWN_BLOCK_ID is not null, 1, 0) as BLOCK_FLAG,
            if(BRAND_NAME = '7-ELEVEN' , count(distinct tmnid), 0) as MAU_7,
            if(BRAND_NAME <> '7-ELEVEN' and MERCHANT_TYPE = 'KA' , count(distinct tmnid), 0) as MAU_KA,
            if(MERCHANT_TYPE = 'NON-KA', count(distinct tmnid), 0) as MAU_LT,
            count(distinct tmnid) as MAU
            from acm-prod.datamart.DM_RETAIL_TRANSACTION
            where 1=1
            and date(trans_datetime) between date_add(current_date(), interval -30 day) and current_date()
            group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
        )
        group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    )
  ) 
  group by 1, 2, 3, 4 
),

wongnai as (
    select   
    TRUETOWN_BLOCK_NAME, 
    TRUETOWN_BLOCK_ID,
    count(*) as MERCHANT_COUNT_WONGNAI
    from (
        select 
        TRUETOWN_BLOCK_NAME,
        TRUETOWN_BLOCK_ID,
        from acm-prod.datamart_bi.DM_WONGNAI_BKK

        UNION ALL

        select 
        TRUETOWN_BLOCK_NAME, 
        TRUETOWN_BLOCK_ID,
        from acm-prod.datamart_bi.DM_WONGNAI_UPC
    )
    group by 1, 2
),

university as (
    select 
    TRUETOWN_BLOCK_NAME, 
    TRUETOWN_BLOCK_ID,
    count(*) as UNIVERSITY_COUNT
    from acm-prod.datamart_bi.DM_ACADEMY_LIST
    group by 1, 2
)

select 
a.TDS_REGION,
a.TDS_PROVINCE,
a.TRUETOWN_BLOCK_NAME,
a.TRUETOWN_BLOCK_ID,
MAU_RETAIL,
MAU_7,
MAU_KA,
MAU_LT,
MERCHANT_COUNT,
MERCHANT_COUNT_7,
MERCHANT_COUNT_KA,
MERCHANT_COUNT_LT,
coalesce(b.MERCHANT_COUNT_WONGNAI, 0) as MERCHANT_COUNT_WONGNAI,
QUALITY_MERCHANT_COUNT,
QUALITY_MERCHANT_COUNT_7,
QUALITY_MERCHANT_COUNT_KA,
QUALITY_MERCHANT_COUNT_LT,
coalesce(c.UNIVERSITY_COUNT, 0) as UNIVERSITY_COUNT,
IN_TRUETOWN_COUNT,
HAVE_BLOCK_ID_COUNT
from without_wongnai as a
left join wongnai as b on a.TRUETOWN_BLOCK_ID = b.TRUETOWN_BLOCK_ID
left join university as c on a.TRUETOWN_BLOCK_ID = c.TRUETOWN_BLOCK_ID
order by 1 desc, 3 desc