WITH
truetown as (
  WITH
  PERCENTILE_MAU_75 as (
    select max(P75) AS P75
    from (
    select PERCENTILE_CONT(MAU_RETAIL, 0.75) OVER() as P75
    from acm-bi.Analysis.NW_TRUETOWN_ADDITION
    )
  ),

  PERCENTILE_MAU_25 as (
    select max(P25) AS P25
    from (
    select PERCENTILE_CONT(MAU_RETAIL, 0.25) OVER() as P25 
    from acm-bi.Analysis.NW_TRUETOWN_ADDITION
    )
  )

    select *, 
    if(MAU_RETAIL > PERCENTILE_MAU_75.P75 and TRUETOWN_BLOCK_ID is not null , 'HIGH',
      if(MAU_RETAIL > PERCENTILE_MAU_25.P25 and MAU_RETAIL <= PERCENTILE_MAU_75.P75 and TRUETOWN_BLOCK_ID is not null, 'MEDIUM',
        if(MAU_RETAIL <= PERCENTILE_MAU_25.P25 and TRUETOWN_BLOCK_ID is not null, 'LOW', 'NONE') 
      )
    ) as BLOCK_ID_ZONE
    from acm-bi.Analysis.NW_TRUETOWN_ADDITION, PERCENTILE_MAU_75, PERCENTILE_MAU_25
)

select TRUETOWN_BLOCK_NAME, TRUETOWN_BLOCK_ID, ADDITION_BLOCK_ID_FLAG
from truetown
where BLOCK_ID_ZONE = 'HIGH'
and (TRUETOWN_BLOCK_ID != '0' and TRUETOWN_BLOCK_ID != '00005-000000' )
order by 2