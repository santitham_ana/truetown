import os
from utils import read_csv, write_to_csv

ROOT_PATH = '/Users/santithamananwattanaporn/Documents/code/my_sql/truetown/new_addition_truetown_script'
SOURCE_FILE = os.path.join(ROOT_PATH, 'source.csv')

VALIDATE_OUTPUT = os.path.join(ROOT_PATH, 'validate.csv')
ADDITION_OUTPUT = os.path.join(ROOT_PATH, 'addition.csv')
def convert_to_row(data, column):
    results = []

    for row in data:
        blocks = [temp.strip() for temp in row[column].split(',')]
        for block in blocks:
            temp = {
                'TRUETOWN_BLOCK_NAME': row['True Smart City']
                ,'TRUETOWN_BLOCK_ID': block
            }
            results.append(temp)
    return results

def main():
    data = read_csv(SOURCE_FILE)

    headers = ['TRUETOWN_BLOCK_NAME', 'TRUETOWN_BLOCK_ID']
    validate_data = convert_to_row(data, 'True Smart City - Happyblock')
    addition_data = convert_to_row(data, 'True Smart City - Additional Happyblock')

    write_to_csv(validate_data, VALIDATE_OUTPUT, headers)
    write_to_csv(addition_data, ADDITION_OUTPUT, headers)

if __name__ == "__main__":
    main()