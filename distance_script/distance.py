import csv
import os
import math
import copy
from utils import read_csv
from utils import write_to_csv
from config import ROOT_PATH

def cal_distance_from_lat_lng(lat1, lon1, lat2, lon2):
    R = 6373.0
    lat1 = math.radians(lat1)
    lon1 = math.radians(lon1)
    lat2 = math.radians(lat2)
    lon2 = math.radians(lon2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = math.sin(dlat / 2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2)**2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    distance = R * c
    return distance

def clean_latlng(data):
    clean_results = []
    for item in data:
        temp_str = ''
        is_replace = False
        for i, c in enumerate(item['TRUETOWN_BLOCK_ID']):
            if i != 0 and not is_replace and c == '-':
                temp_str += '+'
                is_replace = True
            else:
                temp_str += c
        try:
            float(temp_str.split('+')[0])
        except:
            temp_str_split = temp_str.split('+')
            temp_str_split[0] = temp_str_split[0][0] + temp_str_split[0][1:].replace('-', '')
            temp_str = temp_str_split[0] + '+' + temp_str_split[1]
        try:
            float(temp_str.split('+')[1])
        except:
            temp_str_split = temp_str.split('+')
            temp_str_split[1] = temp_str_split[1][0] + temp_str_split[1][1:].replace('-', '')
            temp_str = temp_str_split[0] + '+' + temp_str_split[1]
        item['TRUETOWN_BLOCK_ID'] = temp_str
        clean_results.append(item)
    return clean_results

def cal_distance(data, direction='DOWN'):
    results = copy.deepcopy(data)
    if direction == 'UP':
        results.reverse()
    for index, row in enumerate(results):
        start = False
        row_id = row['TRUETOWN_BLOCK_ID']
        row['LATITUDE'] = float(row['TRUETOWN_BLOCK_ID'].split('+')[0]) / 1000.0
        row['LONGITUDE'] = float(row['TRUETOWN_BLOCK_ID'].split('+')[1]) / 1000.0
        row['LATITUDE_DIFF_' + direction] = -1
        row['LONGITUDE_DIFF_' + direction] = -1
        row['DISTANCE_' + direction] = -1
        row['TRUETOWN_BLOCK_NAME_' + direction] = ''
        if row['TRUETOWN_BLOCK_NAME'] == '':
            for truetown in results[index:]:
                truetown['LATITUDE'] = float(truetown['TRUETOWN_BLOCK_ID'].split('+')[0]) / 1000.0
                truetown['LONGITUDE'] = float(truetown['TRUETOWN_BLOCK_ID'].split('+')[1]) / 1000.0
                if row_id == truetown['TRUETOWN_BLOCK_ID']:
                    start = True
                if start:
                    if truetown['TRUETOWN_BLOCK_NAME'] != '':
                        row['TRUETOWN_BLOCK_NAME_' + direction] = truetown['TRUETOWN_BLOCK_NAME']
                        # row['LATITUDE_DIFF_' + direction] = ((int(truetown['LATITUDE']) - int(row['LATITUDE']))/5.0)*530
                        # row['LONGITUDE_DIFF_' + direction] = ((int(truetown['LONGITUDE']) - int(row['LONGITUDE']))/5.0)*530
                        # print(truetown['LATITUDE'], truetown['LONGITUDE'], row['LATITUDE'], row['LONGITUDE'])
                        row['DISTANCE_' + direction] = cal_distance_from_lat_lng(truetown['LATITUDE'], truetown['LONGITUDE'], row['LATITUDE'], row['LONGITUDE']) * 1000
                        break
    if direction == 'UP':
        results.reverse()
    return results

def select_truetown(data):
    for row in data:
        row['SELECT_TRUETOWN_BLOCK_NAME'] = row['TRUETOWN_BLOCK_NAME']
        row['NEAREST_DISTANCE'] = 0
        if (row['DISTANCE_DOWN'] <= row['DISTANCE_UP'] and row['DISTANCE_DOWN'] != -1) or (row['DISTANCE_UP'] == -1 and row['DISTANCE_DOWN'] != -1):
            row['NEAREST_DISTANCE'] = row['DISTANCE_DOWN']
            row['SELECT_TRUETOWN_BLOCK_NAME'] = row['TRUETOWN_BLOCK_NAME_DOWN']
        elif row['DISTANCE_UP'] <= row['DISTANCE_DOWN'] and row['DISTANCE_UP'] != -1 or (row['DISTANCE_UP'] != -1 and row['DISTANCE_DOWN'] == -1):
            row['NEAREST_DISTANCE'] = row['DISTANCE_UP']
            row['SELECT_TRUETOWN_BLOCK_NAME'] = row['TRUETOWN_BLOCK_NAME_UP']
    return data

def delete_unused_column(data, used_column):
    results = []
    for row in data:
        temp = copy.deepcopy(row)
        for key in row.keys():
            if key not in used_column:
                del temp[key]
        results.append(temp)
    return results

def run_cal_distance(filename = None):
    full_path = filename
    if filename is None:
        filename = input('Please input filename (Only in source): ')
        full_path = os.path.join(ROOT_PATH, 'source', filename)
    data = read_csv(full_path)
    data = clean_latlng(data)
    data = cal_distance(data, 'DOWN')
    data = cal_distance(data, 'UP')

    data = select_truetown(data)

    for row in data:
        row['TRUETOWN_BLOCK_ID'] = row['TRUETOWN_BLOCK_ID'].replace('+', '-')
        
    headers = [
        'TDS_REGION',
        'TDS_PROVINCE',
        'TRUETOWN_BLOCK_NAME',
        'TRUETOWN_BLOCK_ID',
        'MAU_RETAIL',
        'MAU_7',
        'MAU_KA',
        'MAU_LT',
        'MERCHANT_COUNT',
        'MERCHANT_COUNT_7',
        'MERCHANT_COUNT_KA',
        'MERCHANT_COUNT_LT',
        'MERCHANT_COUNT_WONGNAI',
        'QUALITY_MERCHANT_COUNT',
        'QUALITY_MERCHANT_COUNT_7',
        'QUALITY_MERCHANT_COUNT_KA',
        'QUALITY_MERCHANT_COUNT_LT',
        'UNIVERSITY_COUNT',
        'IN_TRUETOWN_COUNT',
        'HAVE_BLOCK_ID_COUNT',
        'BLOCK_ID_ZONE',
        'LATITUDE',
        'LONGITUDE',
        'NEAREST_DISTANCE',
        'SELECT_TRUETOWN_BLOCK_NAME'
    ]

    data = delete_unused_column(data, headers)
    write_to_csv(data, os.path.join(ROOT_PATH, 'distance_output', filename.split('/')[-1]), headers)   
    
if __name__ == "__main__":
    run_cal_distance()
