select min(MERCHANT_COUNT_WONGNAI)
from acm-bi.Analysis.NW_TRUETOWN
where TRUETOWN_BLOCK_ID is not null
and MERCHANT_COUNT_WONGNAI >= 83


select BLOCK_ID_ZONE, count(*)
from (
  select *, 
  if(MAU_RETAIL > 327 and TRUETOWN_BLOCK_ID is not null , 'HIGH',
    if(MAU_RETAIL > 7 and MAU_RETAIL <= 327 and TRUETOWN_BLOCK_ID is not null, 'MEDIUM',
      if(MAU_RETAIL <= 7 and TRUETOWN_BLOCK_ID is not null, 'LOW', 'NONE') 
    )
  ) as BLOCK_ID_ZONE
  from acm-bi.Analysis.NW_TRUETOWN
)
group by 1


select 
PERCENTILE_CONT(MAU_RETAIL, 0.75) OVER() as P75, 
PERCENTILE_CONT(MAU_RETAIL, 0.25) OVER() as P25,
*, 
  if(MAU_RETAIL > 327 and TRUETOWN_BLOCK_ID is not null , 'HIGH',
    if(MAU_RETAIL > 7 and MAU_RETAIL <= 327 and TRUETOWN_BLOCK_ID is not null, 'MEDIUM',
      if(MAU_RETAIL <= 7 and TRUETOWN_BLOCK_ID is not null, 'LOW', 'NONE') 
    )
  ) as BLOCK_ID_ZONE
  from acm-bi.Analysis.NW_TRUETOWN