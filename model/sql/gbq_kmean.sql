--- create model
CREATE OR REPLACE MODEL
  `acm-bi.Analysis.NW_TRUETOWN_WONGNAI_KMEAN` 
  OPTIONS(model_type='kmeans',kmeans_init_method = 'KMEANS++', num_clusters=3, STANDARDIZE_FEATURES=TRUE) AS
--- preprocessing
WITH data AS (
  SELECT 
    key
    ,rating
    ,no_of_rating
    -- ,CASE
    --   WHEN price_range_name = 'ต่ำกว่า 100 บาท' THEN 1
    --   WHEN price_range_name = '101 - 250 บาท' THEN 2
    --   WHEN price_range_name = '251 - 500 บาท' THEN 3
    --   WHEN price_range_name = '501 - 1,000 บาท' THEN 4
    --   ELSE 5
    -- END AS price_rank 
    -- ,price_range_name 
  FROM `acm-bi.Analysis.NW_TRUETOWN_WONGNAI`
  WHERE
    price_range_name IS NOT NULL
    AND rating > 0
    AND no_of_rating > 0
)
SELECT * EXCEPT(key)
FROM data

---predict
SELECT
  * EXCEPT(nearest_centroids_distance)
FROM
  ML.PREDICT( MODEL `acm-bi.Analysis.NW_TRUETOWN_WONGNAI_KMEAN`, (
    SELECT *
    FROM data
  ))


SELECT 
CENTROID_ID
, price_range_name
, COUNT(1)
, min(rating)
, max(rating)
FROM ( 
  SELECT * EXCEPT(price_range_name), 
  CASE
    WHEN price_range_name = 'ต่ำกว่า 100 บาท' THEN '<100'
    WHEN price_range_name = '101 - 250 บาท' THEN '>100'
    ELSE '>250'
  END AS price_range_name
  FROM `acm-bi.Analysis.NW_TRUETOWN_MODEL_KMEAN_3GROUP`
)
GROUP BY CENTROID_ID, price_range_name 
ORDER BY 1,2
