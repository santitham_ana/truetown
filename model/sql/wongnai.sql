SELECT 
  key
  ,name
  ,rating
  ,no_of_rating
  ,category
  ,rank_no
  ,rank_total
  ,rank_name
  ,CASE
    WHEN price_range_name = 'ต่ำกว่า 100 บาท' THEN '1.) <100'
    WHEN price_range_name = '101 - 250 บาท' THEN '2.) 101-250'
    WHEN price_range_name = '251 - 500 บาท' THEN '3.) 251-500'
    WHEN price_range_name = '501 - 1,000 บาท' THEN '4.) 501-1000'
    WHEN price_range_name = 'มากกว่า 1,000 บาท' THEN '5.) >1000'
  END AS price_range_name
  ,CASE
    WHEN price_range_name = 'ต่ำกว่า 100 บาท' THEN 1
    WHEN price_range_name = '101 - 250 บาท' THEN 2
    WHEN price_range_name = '251 - 500 บาท' THEN 3
    WHEN price_range_name = '501 - 1,000 บาท' THEN 4
    WHEN price_range_name = 'มากกว่า 1,000 บาท' THEN 5
  END AS price_range_rank
FROM `acm-bi.Analysis.NW_TRUETOWN_WONGNAI`
WHERE rating > 0 and no_of_rating > 0 and price_range_name IS NOT NULL


SELECT 
label
,price_range_name
,count(1) as total_store
,min(rating) as min_rating
,max(rating) as max_rating
,min(no_of_rating) as min_no_of_rating
,max(no_of_rating) as max_no_of_rating
FROM `acm-bi.Analysis.NW_TRUETOWN_WONGNAI_MODEL_KMEAN_RESULT`
GROUP BY 1,2
ORDER BY 1,2


--- manual label
SELECT
  *
  ,CASE
    WHEN manual_label IN (1,3) THEN 'HIGH POTENTIAL'
    ELSE 'LOW POTENTIAL'
  END AS potential
  ,CASE
    WHEN price_range_rank = 1 THEN '1.) BUDGET'
    WHEN price_range_rank IN (2,3) THEN '2.) MODERATE'
    WHEN price_range_rank IN (4,5) THEN '3.) PREMIUM'
  END AS price_group
FROM (
  SELECT
    * EXCEPT(manual_label)
    ,CASE
      WHEN label IN (4, 1) AND price_range_rank != 1 THEN 1
      WHEN label IN (3,0) AND price_range_rank = 1 THEN 2
      WHEN label IN (4,1) AND price_range_rank = 1 THEN 3
      WHEN label IN (3,0) AND price_range_rank != 1 THEN 4
      WHEN label IN (2) AND price_range_rank = 1 THEN 5
      WHEN label IN (2) AND price_range_rank != 1 THEN 6
    END AS manual_label
  FROM `acm-bi.Analysis.NW_TRUETOWN_WONGNAI_KMEAN_RESULT`
)

SELECT
  manual_label
  ,price_range_name
  ,count(1) as total_store
  ,min(rating) as min_rating
  ,max(rating) as max_rating
  ,min(no_of_rating) as min_no_of_rating
  ,max(no_of_rating) as max_no_of_rating
FROM `acm-bi.Analysis.NW_TRUETOWN_WONGNAI_KMEAN_RESULT`
GROUP BY 1,2
ORDER BY 1,2

--- join to wongnai
SELECT 
  A.*
  ,B.potential
  ,B.price_group
FROM `acm-bi.Analysis.NW_TRUETOWN_WONGNAI` A
LEFT JOIN `acm-bi.Analysis.NW_TRUETOWN_WONGNAI_KMEAN_RESULT` B
ON A.KEY = B.KEY

--- select result
SELECT 
  TRUETOWN_BLOCK_NAME 
  ,TRUETOWN_BLOCK_ID
  ,price_group 
  ,COUNT(1) AS wongnai_shop
FROM `acm-bi.Analysis.NW_TRUETOWN_WONGNAI`
WHERE potential = 'HIGH POTENTIAL'
GROUP BY 1,2,3
ORDER BY 3 DESC,4 DESC



SELECT 
* REPLACE(
  CASE 
    WHEN price_range_name IS NOT NULL AND rating = 0 AND potential IS NULL THEN 'LOW POTENTIAL'
    ELSE potential
  END AS potential
  ,CASE
    WHEN price_range_name IN ('ต่ำกว่า 100 บาท') and price_group IS NULL THEN 'BUDGET'
    WHEN price_range_name IN ('101 - 250 บาท', '251 - 500 บาท') and price_group IS NULL THEN 'MODERATE'
    WHEN price_range_name IN ('501 - 1,000 บาท', 'มากกว่า 1,000 บาท') and price_group IS NULL THEN 'PREMIUM'
    ELSE price_group
  END AS price_group
)
FROM `acm-bi.Analysis.NW_TRUETOWN_WONGNAI`