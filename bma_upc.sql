SELECT 
  name
  ,TRUETOWN_BLOCK_NAME
  ,CASE
    WHEN TRUETOWN_BLOCK_NAME LIKE '%BMA%' THEN 'BMA'
    ELSE 'UPC'
  END AS region
  ,TRUETOWN_BLOCK_ID
  ,province_name
  ,district_name
  ,city_name
  ,phone_no
  ,lat
  ,lng
  ,rating
  ,no_of_rating
  ,category
  ,rank_no
  ,rank_total
  ,rank_name
  ,price_range_name
  ,shop_tier
  ,potential
  ,price_group
FROM `acm-bi.Analysis.NW_TRUETOWN_WONGNAI`
WHERE SELECT_TRUETOWN_BLOCK_NAME IS NOT NULL

--- count bma/upc
SELECT 
  region, COUNT(1)
FROM (
  SELECT DISTINCT TRUETOWN_BLOCK_NAME, region
  FROM `acm-bi.Analysis.NW_TRUETOWN_WONGNAI_54K_TARGET`
)
GROUP BY 1
ORDER BY 1