--- NW_TRUETOWN_WITH_INACTIVE

with 
without_wongnai as (
  select
  TDS_REGION,
  TDS_PROVINCE,
  TRUETOWN_BLOCK_NAME,
  TRUETOWN_BLOCK_ID,
  sum(MAU) as MAU_RETAIL,
  sum(MAU_7) as MAU_7,
  sum(MAU_KA) as MAU_KA,
  sum(MAU_LT) as MAU_LT,
  count(*) as MERCHANT_COUNT,
  sum(INACTIVE_MERCHANT) as INACTIVE_MERCHANT_COUNT,
  sum(ACTIVE_MERCHANT) as ACTIVE_MERCHANT_COUNT,
  sum(QUALITY_MERCHANT) as QUALITY_MERCHANT_COUNT,
  sum(FLAG_7) as MERCHANT_COUNT_7,
  sum(FLAG_KA) as MERCHANT_COUNT_KA,
  sum(FLAG_LT) as MERCHANT_COUNT_LT,
  sum(INACTIVE_MERCHANT_7) as INACTIVE_MERCHANT_COUNT_7,
  sum(INACTIVE_MERCHANT_KA) as INACTIVE_MERCHANT_COUNT_KA,
  sum(INACTIVE_MERCHANT_LT) as INACTIVE_MERCHANT_COUNT_LT,
  sum(ACTIVE_MERCHANT_7) as ACTIVE_MERCHANT_COUNT_7,
  sum(ACTIVE_MERCHANT_KA) as ACTIVE_MERCHANT_COUNT_KA,
  sum(ACTIVE_MERCHANT_LT) as ACTIVE_MERCHANT_COUNT_LT,
  sum(QUALITY_MERCHANT_7) as QUALITY_MERCHANT_COUNT_7,
  sum(QUALITY_MERCHANT_KA) as QUALITY_MERCHANT_COUNT_KA,
  sum(QUALITY_MERCHANT_LT) as QUALITY_MERCHANT_COUNT_LT,
  sum(TRUETOWN_FLAG) as IN_TRUETOWN_COUNT,
  sum(BLOCK_FLAG) as HAVE_BLOCK_ID_COUNT
  from (  
    select 
    *,
    if(MERCHANT_SERVICE = '7-ELEVEN', 1, 0) as FLAG_7,
    if(MERCHANT_SERVICE in ('ORGANIZED', 'MAKRO'), 1, 0) as FLAG_KA,
    if(MERCHANT_SERVICE = 'UNORGANIZED', 1, 0) as FLAG_LT,

    if(MAU < 1, 1, 0) as INACTIVE_MERCHANT,
    if(MERCHANT_SERVICE = '7-ELEVEN' and MAU < 1, 1, 0) as INACTIVE_MERCHANT_7,
    if(MERCHANT_SERVICE in ('ORGANIZED', 'MAKRO') and MAU < 1, 1, 0) as INACTIVE_MERCHANT_KA,
    if(MERCHANT_SERVICE = 'UNORGANIZED' and MAU < 1, 1, 0) as INACTIVE_MERCHANT_LT,

    if(MAU >= 1, 1, 0) as ACTIVE_MERCHANT,
    if(MERCHANT_SERVICE = '7-ELEVEN' and MAU >= 1, 1, 0) as ACTIVE_MERCHANT_7,
    if(MERCHANT_SERVICE in ('ORGANIZED', 'MAKRO') and MAU >= 1, 1, 0) as ACTIVE_MERCHANT_KA,
    if(MERCHANT_SERVICE = 'UNORGANIZED' and MAU >= 1, 1, 0) as ACTIVE_MERCHANT_LT,

    if(MAU >= 5, 1, 0) as QUALITY_MERCHANT,
    if(MERCHANT_SERVICE = '7-ELEVEN' and MAU >= 5, 1, 0) as QUALITY_MERCHANT_7,
    if(MERCHANT_SERVICE in ('ORGANIZED', 'MAKRO') and MAU >= 5, 1, 0) as QUALITY_MERCHANT_KA,
    if(MERCHANT_SERVICE = 'UNORGANIZED' and MAU >= 5, 1, 0) as QUALITY_MERCHANT_LT,
    from (
        select 
        TDS_REGION,
        TDS_PROVINCE,
        TRUETOWN_BLOCK_NAME,
        TRUETOWN_BLOCK_ID,
        MERCHANT_ID,
        SHOP_ID,
        BRAND_NAME,
        MERCHANT_SERVICE,
        TRUETOWN_FLAG,
        BLOCK_FLAG,
        avg(MAU) as MAU,
        avg(MAU_7) as MAU_7,
        avg(MAU_KA) as MAU_KA,
        avg(MAU_LT) as MAU_LT
        from (
            select
            a.TDS_REGION,
            a.TDS_PROVINCE,
            a.TRUETOWN_BLOCK_NAME,
            a.TRUETOWN_BLOCK_ID,
            a.MERCHANT_ID,
            a.SHOP_BRANCH_ID as SHOP_ID,
            a.BRAND_NAME,
            a.MERCHANT_SERVICE,
            if(a.TRUETOWN_BLOCK_NAME is not null, 1, 0) as TRUETOWN_FLAG,
            if(a.TRUETOWN_BLOCK_ID is not null, 1, 0) as BLOCK_FLAG,
            if(a.MERCHANT_SERVICE = '7-ELEVEN', count(distinct tmnid), 0) as MAU_7,
            if(a.MERCHANT_SERVICE in ('ORGANIZED', 'MAKRO'), count(distinct tmnid), 0) as MAU_KA,
            if(MERCHANT_SERVICE = 'UNORGANIZED', count(distinct tmnid), 0) as MAU_LT,
            count(distinct tmnid) as MAU
            from acm-prod.datamart.DM_RETAIL_MASTER_MERCHANT as a
            left join (
            select MERCHANT_ID, SHOP_ID, TMNID
            from acm-prod.datamart.DM_RETAIL_TRANSACTION
            where date(trans_datetime) between date_add(current_date(), interval -30 day) and current_date()
            ) as b
            on a.MERCHANT_ID = b.MERCHANT_ID
            and a.SHOP_BRANCH_ID = b.SHOP_ID
            where 1=1
            group by 1,2,3,4,5,6,7,8,9,10
        )
        group by 1,2,3,4,5,6,7,8,9,10
    )
  ) 
  group by 1,2,3,4 
),

wongnai as (
    select   
    TRUETOWN_BLOCK_NAME, 
    TRUETOWN_BLOCK_ID,
    count(*) as MERCHANT_COUNT_WONGNAI
    from (
        select 
        TRUETOWN_BLOCK_NAME,
        TRUETOWN_BLOCK_ID,
        from acm-prod.datamart_bi.DM_WONGNAI_BKK

        UNION ALL

        select 
        TRUETOWN_BLOCK_NAME, 
        TRUETOWN_BLOCK_ID,
        from acm-prod.datamart_bi.DM_WONGNAI_UPC
    )
    group by 1,2
),

university as (
    select 
    TRUETOWN_BLOCK_NAME, 
    TRUETOWN_BLOCK_ID,
    count(*) as UNIVERSITY_COUNT
    from acm-prod.datamart_bi.DM_ACADEMY_LIST
    group by 1,2
)

select 
a.TDS_REGION,
a.TDS_PROVINCE,
a.TRUETOWN_BLOCK_NAME,
a.TRUETOWN_BLOCK_ID,
MAU_RETAIL,
MAU_7,
MAU_KA,
MAU_LT,
MERCHANT_COUNT,
MERCHANT_COUNT_7,
MERCHANT_COUNT_KA,
MERCHANT_COUNT_LT,
coalesce(b.MERCHANT_COUNT_WONGNAI, 0) as MERCHANT_COUNT_WONGNAI,
INACTIVE_MERCHANT_COUNT,
INACTIVE_MERCHANT_COUNT_7,
INACTIVE_MERCHANT_COUNT_KA,
INACTIVE_MERCHANT_COUNT_LT,
ACTIVE_MERCHANT_COUNT,
ACTIVE_MERCHANT_COUNT_7,
ACTIVE_MERCHANT_COUNT_KA,
ACTIVE_MERCHANT_COUNT_LT,
QUALITY_MERCHANT_COUNT,
QUALITY_MERCHANT_COUNT_7,
QUALITY_MERCHANT_COUNT_KA,
QUALITY_MERCHANT_COUNT_LT,
coalesce(c.UNIVERSITY_COUNT, 0) as UNIVERSITY_COUNT,
IN_TRUETOWN_COUNT,
HAVE_BLOCK_ID_COUNT
from without_wongnai as a
left join wongnai as b on a.TRUETOWN_BLOCK_ID = b.TRUETOWN_BLOCK_ID
left join university as c on a.TRUETOWN_BLOCK_ID = c.TRUETOWN_BLOCK_ID
order by 1 desc, 3 desc