SELECT a.*, truetown.name as TRUETOWN_BLOCK_NAME FROM
(
    SELECT *
         , CONCAT(lpad((CAST(CAST((FLOOR(CAST(lat AS FLOAT64)*100) + (
                IF
                  (MOD(CAST(CAST(lat AS FLOAT64)*1000 AS INT64),10)>5,
                    1,
                    0.5)))*10 AS INT64)AS STRING)),
            5,
            '0'),'-',lpad((CAST(CAST((FLOOR(CAST(lng AS FLOAT64)*100) + (
                IF
                  (MOD(CAST(CAST(lng AS FLOAT64)*1000 AS INT64),10)>=5,
                    0.5,
                    0)))*10 AS INT64)AS STRING)),
            6,
            '0')) AS TRUETOWN_BLOCK_ID
            , 
    FROM `acm-prod.source.BIUPLOAD_WONGNAI_UPC`
) a
/*** True Town ***/
LEFT JOIN source.BIMASTER_TRUETOWN truetown
ON a.TRUETOWN_BLOCK_ID = truetown.BlockID