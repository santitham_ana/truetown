
FILENAME = '/Users/santithamananwattanaporn/Documents/code/my_sql/truetown/model/source/tag.csv'
OUTPUT_FILENAME = '/Users/santithamananwattanaporn/Documents/code/my_sql/truetown/model/source/unique_tag.csv'


def main():
    tag = set()
    with open(FILENAME, 'r') as file:
        for item in file:
            item = item.strip().replace('"', '')
            item = item.split(',')
            for tag_name in item:
                tag.add(tag_name)
    with open(OUTPUT_FILENAME, 'w') as file:
        for item in tag:
            print(item)
            file.write(item.replace('\n', '') + '\n')

if __name__ == "__main__":
    main()
