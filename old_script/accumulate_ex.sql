with temp as (
select DATE("2016-10-02") date_field ,  200 as salary
union all
select DATE("2016-10-09"),  500
union all
select DATE("2016-10-16"),  350
union all
select DATE("2016-10-23"),  400
union all
select DATE("2016-10-30"),  190
union all
select DATE("2016-11-06"),  550
union all
select DATE("2016-11-13"),  610
union all
select DATE("2016-11-20"),  480
union all
select DATE("2016-11-27"),  660
union all
select DATE("2016-12-04"),  690
union all
select DATE("2016-12-11"),  810
union all
select DATE("2016-12-18"),  950
union all
select DATE("2016-12-25"),  1020
union all
select DATE("2017-01-01"),  680
) 
SELECT *,SUM(salary) OVER(ORDER BY salary) FROM temp