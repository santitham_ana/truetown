summary
python3 summary.py --interactive --clipboard_mode

--interactive 
for interactive shell select mode
1) Summary
2) Truetown Unique Name (Print Unique truwtown name from input file)

--clipboard_mode
copy outout to clipboard (Only support Truetown Unique Name and Block/Wongnai Added Summary)

Ex
python3 summary.py --interactive --clipboard_mode
Which mode do you prefer 1) Summary 2) Truetown Name List: 2 <- Input in number format 1 or 2
Please input filename: criteria3.csv <- file must in source folder

python3 summary.py
run all summary from all high_segment,criteria2,criteria3 it open file from source folder and
filter distances are 1 km, 2 km, 3 km, 5 km, 10 km