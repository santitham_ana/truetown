WITH
truetown as (
  select *, 
  if(MAU_RETAIL > 545 and TRUETOWN_BLOCK_ID is not null , 'HIGH',
    if(MAU_RETAIL > 11 and MAU_RETAIL <= 545 and TRUETOWN_BLOCK_ID is not null, 'MEDIUM',
      if(MAU_RETAIL <= 11 and TRUETOWN_BLOCK_ID is not null, 'LOW', 'NONE') 
    )
  ) as BLOCK_ID_ZONE
  from (
  SELECT 
    A.* EXCEPT(TRUETOWN_BLOCK_NAME)
    ,IF(A.TRUETOWN_BLOCK_NAME IS NULL AND B.TRUETOWN_BLOCK_NAME IS NOT NULL, REGEXP_REPLACE(B.TRUETOWN_BLOCK_NAME, '\\s\\s+', ' '), REGEXP_REPLACE(A.TRUETOWN_BLOCK_NAME, '\\s\\s+', ' ')) AS TRUETOWN_BLOCK_NAME
    ,IF(B.TRUETOWN_BLOCK_ID IS NOT NULL, 1, 0) AS ADDITION_BLOCK_ID_FLAG
  FROM `acm-bi.Analysis.NW_TRUETOWN_BAK_20200828` A
  LEFT JOIN (
    SELECT * FROM `acm-bi.Analysis.SA_20200828_TRUETOWN_ADDITION_BLOCKID_ADDITION`
    UNION ALL
    SELECT * FROM `acm-bi.Analysis.SA_20200828_TRUETOWN_ADDITION_BLOCKID_VALIDATE`
  ) B
  ON A.TRUETOWN_BLOCK_ID = B.TRUETOWN_BLOCK_ID  
  )
),

sum_high_seg as (
  select sum(MERCHANT_COUNT_WONGNAI) as WONGNAI, sum(MERCHANT_COUNT_LT) as LONGTAIL, count(*) as BLOCK_ID, count(distinct TRUETOWN_BLOCK_NAME) as TRUETOWN
  from truetown
  where BLOCK_ID_ZONE = 'HIGH'
--   and TRUETOWN_BLOCK_NAME is not null
  group by BLOCK_ID_ZONE
),

-- select count(distinct TRUETOWN_BLOCK_NAME) as TRUETOWN, count(*) as BLOCK_ID, sum(MERCHANT_COUNT_WONGNAI) as WONGNAI
-- from truetown
-- where TRUETOWN_BLOCK_NAME is not null

-- select PERCENTILE_CONT(MERCHANT_COUNT_WONGNAI, 0.90) OVER()
-- from truetown
-- where BLOCK_ID_ZONE = 'HIGH'
-- and TRUETOWN_BLOCK_ID != '0'
-- and MERCHANT_COUNT_WONGNAI != 0

-- select PERCENTILE_CONT(MAU_RETAIL, 0.75) OVER() as P75, PERCENTILE_CONT(MAU_RETAIL, 0.25) OVER() as P25 
-- from truetown

pnok_solu as (
  select sum(MERCHANT_COUNT_WONGNAI) as WONGNAI, sum(MERCHANT_COUNT_LT) as LONGTAIL, count(*) as BLOCK_ID, count(distinct TRUETOWN_BLOCK_NAME) as TRUETOWN
  from (
    select *
    from (
      select sum(MERCHANT_COUNT_WONGNAI) OVER(order by MERCHANT_COUNT_WONGNAI desc) as SUM_WONG, *
      from truetown
      where BLOCK_ID_ZONE = 'HIGH'
    ) where SUM_WONG <= 50000
--     and TRUETOWN_BLOCK_NAME is not null
  )
),

new_solu as (
  select sum(MERCHANT_COUNT_WONGNAI) as WONGNAI, sum(MERCHANT_COUNT_LT) as LONGTAIL, count(*) as BLOCK_ID, count(distinct TRUETOWN_BLOCK_NAME) as TRUETOWN
  from (
    select *
    from truetown
    where (MERCHANT_COUNT_WONGNAI != 0 and MERCHANT_COUNT_WONGNAI >= 83)
    and (TRUETOWN_BLOCK_ID != '0' and TRUETOWN_BLOCK_ID != '00005-000000' )
    and BLOCK_ID_ZONE = 'HIGH'
--     and TRUETOWN_BLOCK_NAME is not null
  )
)

  select TRUETOWN_BLOCK_NAME , TRUETOWN_BLOCK_ID,ADDITION_BLOCK_ID_FLAG ,MERCHANT_COUNT_WONGNAI
  from truetown
  where BLOCK_ID_ZONE = 'HIGH'

ORDER BY TRUETOWN_BLOCK_ID